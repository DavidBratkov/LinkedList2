#include "Node.h"

//void release_node(struct node*);

int delete(struct node *LL, int number){

	int last;

	struct node *prev;

	if (LL -> next == NULL) return(0);

	while (LL -> next != NULL && LL -> data != number){
		if ((LL -> next) -> data == number) prev = LL;
		LL = LL -> next;
	}

	if (number == LL -> data){
		prev -> next = LL -> next;
		free(LL);//release_node(LL);

		return(1);
	}
	return(0);
}
