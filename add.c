#include "Node.h"
	
int add(struct node *current,int number){

	int flag=0;
       
	if (current -> next == NULL) flag=1; //checks to see if its the first node on the list
	
	else if (number < (current -> next) -> data) flag=4; //is checking to see if it needs to be put before the first input
	
	while (flag == 0){
		//printf("DEBUG-ADD flag:%i\n", flag);
		current = current -> next;
		if (current -> next == NULL) flag=2;
                else if (number < (current -> next) -> data) flag=3;

	}
		
	if (flag != 0){ // flag:1=only sentinel node flag:2=last node in list flag:3=The next node has a larger number flag:4=same as flag 3 but it checks at the sentinel node
		//printf("DEBUG-ADD flag:%i\n", flag);
		//add the number the list
		struct node *new = malloc(sizeof(struct node));
		new -> next = current -> next;
		current -> next = new; 	
		new -> data = number;
		return(1);
	}
	
	return(0);
}
