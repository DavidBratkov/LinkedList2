PA2: PA2.o delete.o add.o init.o print.o search.o Node.h
	gcc -o PA2 PA2.o delete.o add.o init.o print.o search.o

PA2.o : PA2.c delete.c add.c init.c print.c search.c Node.h
	gcc -c PA2.c delete.c add.c init.c print.c search.c

delete.o: delete.c Node.h 
	gcc -c delete.c

add.o: add.c Node.h
	gcc -c add.c

print.o: print.c Node.h
	gcc -c print.c

search.o: search.c Node.h
	gcc -c search.c

init.o: init.c Node.h
	gcc -c init.c

clean:
	rm PA2 PA2.o delete.o add.o print.o search.o init.o
