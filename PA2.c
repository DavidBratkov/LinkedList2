// This is Programing assignment 2 by David Bratkov
//
// CSE 222
//
// 3/3/2020
//
// This program basically is building a linked list that the user can manipulate with given commands
// This is a revised code from PA1 Make sure to fix the mistake from PA1

#include "Node.h"

void *init(); // Revised idk if works
int add(struct node*,int); // revised idk if it works
void print(struct node*); // revised idk if it works
int delete(struct node*,int); // revised idk if it works
int search(struct node*,int); // revised idk if it works
void releasenode(struct node*); // a function at the end of the main function that releases all the nodes in the linked list


int main() {
	struct node *LL=init();

	printf("Hello and welcome to David Bratkov's PA2\nHere you will need to type in a few commands\nEach command will help you manipulate a list of data\n");
	printf("The format should be as follows:i 5*enter* to insert 5\n");

	char command=0;
	char temp[120], temp1[120];
	int number, rvalue, flag;

	while (command != 'x'){ // This is the main loop 
		command=0;
		number=0;
		flag=0;
		while (flag == 0){ // this is the loop for inputs
			printf(">");
			fgets(temp, 120, stdin);
			rvalue=sscanf(temp, "%c%i%s", &command, &number, temp1);//the main sscanf function
			//printf("DEBUG command:%c number:%i\n",command, number);
			if (command == 'x'){
			releasenode(LL);
			flag=-2;
			}
			
			if (command != 'i' && command != 'p' && command != 's' && command != 'd' && command != 'x') printf("The commands are i(insert), p(print), s(search), d(delete), x(exit)\n");
			
			if (command == 'i' || command == 'p' || command == 's' || command == 'd') flag=1;

			if (flag == 1){//this function is to check whenever there is a number after the command
			
			int test=0;
			if (command == 'i') test=sscanf(temp,"i %i", &number);
			if (command == 's') test=sscanf(temp,"s %i", &number);
			if (command == 'd') test=sscanf(temp,"d %i", &number);
			//printf("DEBUG test:%i command:%c\n", test, command);
			if (test != 1 && command != 'p') printf("Please input a number after the command\n");
			if (test != 1) flag=0;
			//if (command == 'p' || command == 'x' || command == 'z') flag=1;
			if (command == 'p') flag=1;
	
	
		}
	
	
	}
	
	//printf("DEBUG It left the loop! command=%c\n", command);
	
	if (command == 'p'){ // print command
		if (LL -> next != NULL) print(LL);
		if (LL -> next == NULL) printf("The List is empty!\n"); // there is no need to print if its empty
	}
	//printf("WHY?!?!\n");
	
	int error=-1, searchret=0; // two temp variables one to see if the function returns with an error and the other is for the search function
	if (command == 'i'){ // insert command
		//printf("DEBUG it went in the insert function\n");
		if (LL -> next != NULL) searchret=search(LL,number);// searches to see if it's already in the list 
		
		if (searchret == 0){ // it wont try to add if the search function found the same number
		//printf("DEBUG It got to the insert function!\n");
		error=add(LL,number);
			if(error == 0) printf("Out of Space!\n");
			if(error == 1) printf("Success!\n");
		}
		if (searchret == 1) printf("Number already in list\n");
	}
		
	
	if (command == 's'){ //search command
		searchret=2;
		if (LL -> next == NULL) printf("Not found!\n");
		if (LL -> next != NULL) searchret=search(LL,number);
		if (searchret == 0) printf("Not Found\n");
		if (searchret == 1) printf("Found\n");
	}
	
	int delret=2; // temp variable for the delete function
	if (command == 'd'){
		searchret=2;
		if (LL -> next == NULL) printf("Node not found!\n");
		if (LL -> next != NULL) searchret=search(LL,number); //searches before hand to find if it has the same node in the list
		if (searchret == 0) printf("Node not found!\n");
		if (searchret == 1){
			delret=delete(LL,number);
			if (delret == 1) printf("Success!\n");
			//if (delret == 0) printf("Error this shouldnt happen\n"); 
		}
	}
	
	
	}//closing bracket of the while != x


}

void releasenode(struct node *first){ //this is a function to free all the nodes at the end
        struct node *prev;
        while (first -> next != NULL){
                prev=first;
                first=first -> next;
                free(prev);
        }
        free(first);
}
